﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;

public class NextButton : MonoBehaviour
{
    public Button playButton;
    public static bool isPlaced;
    
    public void Start()
    {
        isPlaced = false;
        playButton.interactable = false;

        playButton.onClick.AddListener(PlayGame);
    }

    private void Update()
    {
        if (isPlaced)
        {
            playButton.interactable = true;
        }
    }

    public void PlayGame()
    {
        SpawnObjectOnPlane.isPlaced = true;
        SpawnObjectOnPlane.SetAllPlanesActive(false);
    }
}