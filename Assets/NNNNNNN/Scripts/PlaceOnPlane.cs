﻿using System.Collections;
using System.Collections.Generic;
using Lean.Touch;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UIElements;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using Button = UnityEngine.UI.Button;

/// <summary>
/// Listens for touch events and performs an AR raycast from the screen touch point.
/// AR raycasts will only hit detected trackables like feature points and planes.
///
/// If a raycast hits a trackable, the <see cref="placedPrefab"/> is instantiated
/// and moved to the hit position.
/// </summary>
[RequireComponent(typeof(ARRaycastManager))]
public class PlaceOnPlane : MonoBehaviour
{
    Vector2 startPos, endPos, direction; // touch start position, touch end position, swipe direction
    float touchTimeStart, touchTimeFinish, timeInterval; // to calculate swipe time to sontrol throw force in Z direction

    [SerializeField]
    float throwForceInXandZ = 0.0001f; // to control throw force in X and Y directions

    [SerializeField]
    float throwForceInY = 0.00050f;

    [SerializeField]
    [Tooltip("Instantiates this prefab on a plane at the touch location.")]
    GameObject m_PlacedPrefab;

    /// <summary>
    /// The prefab to instantiate on touch.
    /// </summary>
    public GameObject placedPrefab
    {
        get { return m_PlacedPrefab; }
        set { m_PlacedPrefab = value; }
    }

    /// <summary>
    /// The object instantiated as a result of a successful raycast intersection with a plane.
    /// </summary>
    public static GameObject spawnedObject { get; private set; }
    public Camera arCamera;
    private Color oldColor;
    private Color attackColor;
    private bool moving = false;
    private bool isPlaced;
    public Button playGameButton;
    public GameObject playGame;

    private float oldHeight;

    private float maxHeight = 0.00000000000001f;

    private GameObject player;
    private GameObject playingFigure;
    private List<Vector3> oldPos;

    private GameObject mark;

    private GameObject dice;
    private static Rigidbody rb;
    private static Vector3 diceVelocity;

    public static bool turnDice = false;

    private bool hasLanded = false;

    public static int diceNumber = 0;
    
    private static WaypointController controller;

    private bool playingFigureisPlaced;

    public GameObject PlayerView;

    private bool down = false;

    private bool hasSword = false;
    private bool swingSword = false;

    private float swordHeight = 0;

    public static Color choosenColor;
    
    void Awake()
    {
        playingFigureisPlaced = false;
        oldPos = new List<Vector3>();
        isPlaced = false;
        playGameButton.interactable = false;
        playGameButton.onClick.AddListener(PlayGame);
        m_RaycastManager = GetComponent<ARRaycastManager>();
        oldColor = new Color(0.96f, 0.46f, 0.078f);
        attackColor = Color.red;
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
#if UNITY_EDITOR
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {           
            var mousePosition = Input.mousePosition;
            touchPosition = new Vector2(mousePosition.x, mousePosition.y);
            return true;
        }
#else
        if (Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }
#endif
        touchPosition = default;
        return false;
    }

    void Update()
    {

        if (swingSword)
        {
            if (swordHeight > oldPos[2].y - 0.1f)
                swordHeight -= 0.03f;
        }
        else if (!swingSword && hasSword)
        {
            if (swordHeight < oldPos[2].y)
                swordHeight += 0.03f;
        }
        
        if (dice != null)
        {
            if (dice.transform.position.y < -5f)
            {
                dice.transform.position = new Vector3(spawnedObject.transform.position.x, spawnedObject.transform.position.y + 0.1f, spawnedObject.transform.position.z);
            }
        }

        if (rb != null)
        {
            if (rb.IsSleeping() && hasLanded)
            {
                hasLanded = false;
                Debug.Log(PlaceOnPlane.diceNumber);
                SetTargetWayPointJumanji(PlaceOnPlane.diceNumber);
            }
        }

        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;
        
        Ray ray = arCamera.ScreenPointToRay(touchPosition);
        RaycastHit hitObject;

        for (int i = 1; i < Input.touchCount; i++)
        {
            Touch t = Input.GetTouch(i);

            if (t.phase == TouchPhase.Began)
            {
                if (player != null && player.transform.name.Contains("Sword"))
                {
                    if (hasSword)
                    {
                        swingSword = true;
                    }
                }
            }
            
            if (t.phase == TouchPhase.Ended)
            {
                if (player != null && player.transform.name.Contains("Sword"))
                {
                    if (hasSword)
                    {
                        swingSword = false;
                    }
                }
            }
        }

        if (Input.GetTouch(0).phase == TouchPhase.Began && !moving)
        {
            if (Physics.Raycast(ray, out hitObject))
            {
                Debug.Log(hitObject.transform.tag);
                switch (hitObject.transform.tag)
                {
                    case "Dice":
                        RandomCube.diceNumber = 0;
                        hasLanded = false;
                        touchTimeStart = Time.time;
                        startPos = Input.GetTouch (0).position;
                        SetNewPlayerObject(hitObject.transform.gameObject);
                        player.GetComponent<DiceScript>().enabled = false;
                        //player.GetComponent<Rigidbody>().isKinematic = true;
                        rb.useGravity = false;
                        //player.GetComponent<Rigidbody>().useGravity = false;
                        player.transform.position = new Vector3(player.transform.position.x, 
                            player.transform.position.y + 0.3f, 
                            player.transform.position.z);
                        
                        player.GetComponent<Outline>().enabled = true;
                        player.GetComponent<Outline>().OutlineColor = oldColor;
                        choosenColor = oldColor;
                        player.GetComponent<LeanTouch>().enabled = true;
                        player.GetComponent<LeanTwistRotateAxis>().enabled = true;
                        break;
                    case "Weapon":
                        SetNewPlayerObject(hitObject.transform.gameObject);

                        if (player.transform.name.Contains("Sword"))
                            hasSword = true;
                        
                        player.GetComponent<Outline>().enabled = true;
                        player.GetComponent<Outline>().OutlineColor = oldColor;
                        choosenColor = oldColor;
                        player.GetComponent<LeanTouch>().enabled = true;
                        player.GetComponent<LeanTwistRotateAxis>().enabled = true;
                        break;
                    case "PlayerFigure":
                        
                        if (playingFigureisPlaced)
                        {
                            break;
                        }
                        SetNewPlayerObject(hitObject.transform.gameObject);
                        player.GetComponent<Outline>().enabled = true;
                        player.GetComponent<Outline>().OutlineColor = oldColor;
                        choosenColor = oldColor;
                        player.GetComponent<LeanTouch>().enabled = true;
                        player.GetComponent<LeanTwistRotateAxis>().enabled = true;
                        break;
                    case "Jumanji":

                        if (isPlaced)
                        {
                            break;
                        }

                        SetNewPlayerObject(hitObject.transform.gameObject);
                        player.GetComponent<Outline>().enabled = true;
                        player.GetComponent<Outline>().OutlineColor = oldColor;
                        choosenColor = oldColor;
                        player.GetComponent<LeanTouch>().enabled = true;
                        player.GetComponent<LeanTwistRotateAxis>().enabled = true;
                        break;
                    default:
                        break;
                }
            }
        }

        if (!IsPointOverUIObject(touchPosition) && m_RaycastManager.Raycast(touchPosition, s_Hits, 
                                                    TrackableType.PlaneWithinPolygon)
                                                && EventSystem.current.IsPointerOverGameObject() == false)
        {
            // Raycast hits are sorted by distance, so the first one
            // will be the closest hit.
            var hitPose = s_Hits[0].pose;

            if (spawnedObject == null)
            {
                spawnedObject = Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation);
                spawnedObject.transform.rotation = Quaternion.identity;
                oldPos.Add(spawnedObject.transform.position);
                playGameButton.interactable = true;
            }
            else
            {
                if (moving)
                {
                    player.GetComponent<Outline>().enabled = true;
                    player.GetComponent<Outline>().OutlineColor = choosenColor ;
                    player.GetComponent<LeanTouch>().enabled = true;
                    player.GetComponent<LeanTwistRotateAxis>().enabled = true;
                    
                    if (player != null)
                    {
                        if (player.transform.tag.Contains("Dice"))
                        {
                            player.transform.position = new Vector3(hitPose.position.x, 
                                player.transform.position.y, hitPose.position.z);
                        }
                        else
                        {
                            player.transform.position = hitPose.position;
                        }

                        if (player.transform.tag.Contains("PlayerFigure"))
                        {
                            if (Physics.Raycast(ray, out hitObject))
                            {
                                Debug.Log(hitObject.transform.tag);
                                
                                if (hitObject.transform.tag.Contains("Mark"))
                                {
                                    BoxCollider[] boxColliders = player.GetComponents<BoxCollider>();
                                    boxColliders[0].enabled = false;
                                    boxColliders[1].enabled = false;
                                    down = true;
                                    mark = hitObject.transform.gameObject;
                                    mark.GetComponent<Renderer>().material.SetColor("_Color",Color.yellow); 
                                }
                                else
                                {
                                    if (mark != null)
                                    {
                                        BoxCollider[] boxColliders = player.GetComponents<BoxCollider>();
                                        boxColliders[0].enabled = true;
                                        boxColliders[1].enabled = true;
                                        down = false;
                                        mark.GetComponent<Renderer>().material.SetColor("_Color", new Color(0.35f, 1f, 0.19f)); 
                                    }
                                }
                                
                                if (down)
                                {
                                    player.transform.position = new Vector3(hitPose.position.x, oldPos[0].y - 0.1f,
                                        hitPose.position.z);
                                }
                                else if (!down)
                                {
                                    player.transform.position = new Vector3(hitPose.position.x, oldPos[0].y,
                                        hitPose.position.z);

                                }
                            }
                        }
                          
                        if (player.transform.tag.Contains("Weapon"))
                        {
                            if (player.transform.name.Contains("Sword"))
                            {
                                player.transform.position = new Vector3(hitPose.position.x, swordHeight, hitPose.position.z);
                            }
                            else if (player.transform.name.Contains("Shield"))
                            {
                                player.transform.position = new Vector3(hitPose.position.x, oldPos[3].y , hitPose.position.z);
                            }
                        }
                    }
                }
            }
        }
        
        if (Input.GetTouch(0).phase == TouchPhase.Ended)
        {
            touchTimeFinish = Time.time;
            // calculate swipe time interval 
            timeInterval = touchTimeFinish - touchTimeStart;

            // getting release finger position
            endPos = Input.GetTouch (0).position;

            // calculating swipe direction in 2D space
            direction = startPos - endPos;
            
            if (player != null)
            {
                player.GetComponent<LeanTouch>().enabled = false;
                player.GetComponent<LeanTwistRotateAxis>().enabled = false;
                player.GetComponent<Outline>().enabled = false;
                moving = false;
                player.GetComponent<Outline>().OutlineColor = oldColor;

                if (player.transform.tag.Contains("Dice"))
                {
                    // add force to balls rigidbody in 3D space depending on swipe time, direction and throw forces
                    
                    player.GetComponent<DiceScript>().enabled = true;
                    hasLanded = true;
                    rb.useGravity = true;
                    rb.AddForce (- direction.x * throwForceInXandZ, - direction.y * throwForceInY, throwForceInXandZ / timeInterval);
                    rb.AddTorque(Random.Range(0, 500), Random.Range(0, 500), Random.Range(0, 500));
                }

                if (player.transform.tag.Contains("PlayerFigure"))
                {
                    if (Physics.Raycast(ray, out hitObject))
                    {
                        if (hitObject.transform.tag.Contains("Mark"))
                        {
                            player.transform.position = hitObject.transform.position;
                            
                            if (player.transform.name.Contains("HorseStatue"))
                            {
                                oldPos[0] = player.transform.position;
                                spawnedObject.transform.GetChild(5).gameObject.SetActive(false);
                                player.transform.GetChild(0).rotation = Quaternion.Euler(0, 90f, 0);
                                player.transform.GetChild(0).localScale = new Vector3(0.0002f, 0.0002f, 0.0002f);
                            }
                            else if (player.transform.name.Contains("LionStatue"))
                            {
                                oldPos[1] = player.transform.position;
                                spawnedObject.transform.GetChild(4).gameObject.SetActive(false);
                                player.transform.GetChild(0).rotation = Quaternion.Euler(0, 90f, 0);
                                player.transform.GetChild(0).localScale = new Vector3(0.04f, 0.04f, 0.04f);
                            }

                            
                            BoxCollider[] boxColliders = player.GetComponents<BoxCollider>();
                            boxColliders[1].enabled = true;
                            PlayerView.SetActive(true);
                            playingFigureisPlaced = true;
                            playingFigure = player;
                            controller = playingFigure.GetComponent<WaypointController>();
                            player.transform.GetComponent<HitSpear>().enabled = true;
                            hitObject.transform.gameObject.SetActive(false);
                            spawnedObject.transform.GetChild(6).gameObject.SetActive(false);
                            spawnedObject.transform.GetChild(8).gameObject.SetActive(true);
                            dice = spawnedObject.transform.GetChild(8).gameObject;
                            rb = dice.GetComponent<Rigidbody>();
                            spawnedObject.transform.GetChild(9).gameObject.SetActive(true);
                            spawnedObject.transform.GetChild(10).gameObject.SetActive(true);
                            oldPos.Add(spawnedObject.transform.GetChild(9).position);
                            oldPos.Add(spawnedObject.transform.GetChild(10).position);
                            swordHeight = oldPos[2].y;
                        }
                        else
                        {
                            if (player.transform.name.Contains("HorseStatue"))
                            {
                                player.transform.position = oldPos[0];
                            }
                            else if (player.transform.name.Contains("LionStatue"))
                            {
                                player.transform.position = oldPos[1];
                            }
                        }
                    }
                }

                if (player.transform.tag.Contains("Weapon"))
                {
                    if (Physics.Raycast(ray, out hitObject))
                    {
                        if (player.transform.name.Contains("Sword"))
                        {
                            player.transform.position = oldPos[2];
                            hasSword = false;
                        }
                        else if (player.transform.name.Contains("Shield"))
                        {
                            player.transform.position = oldPos[3];
                        }
                    }
                }
                player = null;
            }
        }
    }

    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    ARRaycastManager m_RaycastManager;
    
    bool IsPointOverUIObject(Vector2 pos)
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return false;

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(pos.x, pos.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;
    }
    
    public void PlayGame()
    {
        GetComponent<ARPlaneManager>().enabled = false;
        isPlaced = true;
        playGame.SetActive(false);
        BoxCollider[] boxColliders = spawnedObject.GetComponents<BoxCollider>();
        boxColliders[0].enabled = false;
        spawnedObject.transform.GetChild(2).gameObject.SetActive(true);
        spawnedObject.transform.GetChild(3).gameObject.SetActive(true);
        spawnedObject.transform.GetChild(4).gameObject.SetActive(true);
        spawnedObject.transform.GetChild(5).gameObject.SetActive(true);
        spawnedObject.transform.GetChild(6).gameObject.SetActive(true);
        spawnedObject.transform.GetChild(7).gameObject.SetActive(true);
        oldPos.Clear();
        oldPos.Add(spawnedObject.transform.GetChild(4).position);
        oldPos.Add(spawnedObject.transform.GetChild(5).position);
    }

    public void SetNewPlayerObject(GameObject hitObject)
    {
        player = hitObject;
        moving = true;
    }
    
    public void SetTargetWayPointJumanji(int n)
    {
        if (n != 0)
        {
            if (controller.GetLastWaypointIndex() + n < controller.waypoints.Count)
            {
                controller.SetTargetWaypoint(controller.waypoints[controller.GetTargetWaypointIndex()]);
                controller.IncreaseLastWayPointIndex(n);
            }
        }
    }
}
