﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HitSword : MonoBehaviour
{
    private Color oldColor;
    private Color attackColor;
    private void Start()
    {
        oldColor = new Color(0.96f, 0.46f, 0.078f);
        attackColor = Color.red;    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Weapon"))
        {
            PlaceOnPlane.choosenColor = attackColor;
            transform.GetComponent<Outline>().enabled = true;

            if (transform.tag.Contains("Spider"))
            {
                Spider _spider = transform.gameObject.GetComponent<Spider>();
                _spider.Die();
            }
            else if (transform.tag.Contains("Skeleton"))
            {
                Skeleton _skeleton = transform.gameObject.GetComponent<Skeleton>();
                _skeleton.GetComponent<CapsuleCollider>().enabled = false;
                _skeleton.Die();
            }
            StartCoroutine(ResetSword(other));
        }
    }

    IEnumerator ResetSword(Collider other)
    {
        yield return new WaitForSecondsRealtime(0.5f);
        PlaceOnPlane.choosenColor = oldColor;
        transform.GetComponent<Outline>().enabled = false;
    }
}
