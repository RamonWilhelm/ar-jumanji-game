﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;
using UnityEngine.UI;
//using UnityEngine.XR.ARFoundation;

public class Test : MonoBehaviour
{
    private int tapCount = 0;
    private GameObject placement;
    public Camera arCamera;
    private float doubleTapTimer = 0;
    private Color oldColor;

    private float range;
    private bool movingVertical = false;
    private bool moving = false;

    private void Start()
    {
        range = 1000f;
        oldColor = new Color(0.96f, 0.46f, 0.078f);
    }

    void Update()
    {
        doubleTapTimer += Time.deltaTime;
        
        for (int i = 0; i < Input.touchCount; i++)
        {
            Ray ray = arCamera.ScreenPointToRay(Input.GetTouch(i).position);
            RaycastHit hitObject;
            
            // One-Finger Touch one time
            if (i == 0)
            {
                if (Input.GetTouch(i).phase == TouchPhase.Began)
                {
                    tapCount += 1;
                }

                if (tapCount == 0 && !moving)
                {
                    if (Physics.Raycast(ray, out hitObject, range))
                    {
                        if (hitObject.transform.name != null)
                            Debug.Log(hitObject.transform.name);
                        
                        switch (hitObject.transform.name)
                        {
                            case "Cube":
                            case "WhiteDice":
                            case "Shield":
                                ActivateObject(hitObject);
                                moving = true;

                                break;
                            default:
                                ResetObject(placement);
                                break;
                        }
                    }
                }

                // One-Finger Touch two times

                if (tapCount == 1 && doubleTapTimer < 0.5f && !(moving))
                {

                    if (Physics.Raycast(ray, out hitObject, range))
                    {

                        switch (hitObject.transform.name)
                        {
                            case "Cube":
                            case "WhiteDice":
                            case "Shield":
                                movingVertical = true;
                                placement.GetComponent<Outline>().OutlineColor = Color.blue;
                                break;
                            default:
                                break;
                        }
                    }

                }


                if (moving)
                {
                    if (Physics.Raycast(ray, out hitObject, range))
                    {
                        if (placement.name.Contains("WhiteDice") && 
                            placement.GetComponent<DiceScript>() != null &&
                            placement.GetComponent<Rigidbody>() != null)
                        {
                            placement.GetComponent<DiceScript>().enabled = false;
                            placement.GetComponent<Rigidbody>().isKinematic = true;
                        }
                        
                        if (!movingVertical)
                        {
                            placement.transform.position = new Vector3(hitObject.point.x,
                                placement.transform.position.y, hitObject.point.z);
                        }
                        else if (movingVertical)
                        {
                            placement.GetComponent<Outline>().OutlineColor = Color.blue;
                            placement.transform.position = new Vector3(placement.transform.position.x,
                                hitObject.point.y, hitObject.point.z);
                        }

                    }
                }

                if (doubleTapTimer > 0.5f)
                {
                    doubleTapTimer = 0f;
                    tapCount = 0;
                }
            }

            // Double Finger Touch one times
/*            
            if (i == 1)
            {
                if (tapCount == 1)
                {
                    if (Physics.Raycast(ray, out hitObject, range))
                    {

                        switch (hitObject.transform.name)
                        {
                            case "Cube":
                            case "WhiteDice":
                                placement.GetComponent<Outline>().OutlineColor = Color.red;
                                break;

                            default:
                                if (placement != null)
                                {
                                    ResetObject(placement);
                                }

                                break;
                        }
                    }
                }
            }
*/

            if (Input.GetTouch(i).phase == TouchPhase.Ended)
            {
                moving = false;
                movingVertical = false;
                if (placement != null)
                {
                    placement.GetComponent<Outline>().enabled = false;
                }

                if (movingVertical)
                {
                    tapCount = 0;
                    doubleTapTimer = 0.0f;
                }
                
                if (placement.name.Contains("WhiteDice") && 
                    placement.GetComponent<DiceScript>() != null &&
                    placement.GetComponent<Rigidbody>() != null)
                {
                    placement.GetComponent<DiceScript>().enabled = true;
                    placement.GetComponent<Rigidbody>().isKinematic = false;
                }
            }
        }
    }

    public void ActivateObject(RaycastHit hitObject)
    {
        ResetObject(placement);
        doubleTapTimer = 0f;
        placement = hitObject.transform.gameObject;
        placement.GetComponent<Outline>().enabled = true;
        placement.GetComponent<Outline>().OutlineColor = oldColor;
    }
    
    public void ResetObject(GameObject placement)
    {
        if (placement != null)
        {
            placement.GetComponent<Outline>().OutlineColor = oldColor;
            placement.GetComponent<Outline>().enabled = false;
            placement = null;
            moving = false;
            tapCount = 0;
            doubleTapTimer = 0f;
        }
    }
}
