﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class StartMenuButtons : MonoBehaviour
{
    public Button exit;
    public Button instruction;
    public Button start;
    public GameObject startMenu;
    public GameObject instructionTextView;
    public GameObject planeGenerationMenu;
    public GameObject arSessionOrigin;

    public void Start()
    {
        arSessionOrigin.GetComponent<ARPlaneManager>().enabled = false;
        exit.onClick.AddListener(CloseGame);
        instruction.onClick.AddListener(ShowInstruction);
        start.onClick.AddListener(StartGame);
    }

    public void CloseGame()
    {
        Application.Quit();
    }
    
    public void StartGame()
    {
        arSessionOrigin.GetComponent<ARPlaneManager>().enabled = true;
        startMenu.SetActive(false);
        planeGenerationMenu.SetActive(true);
    }

    public void ShowInstruction()
    {
        startMenu.SetActive(false);
        instructionTextView.SetActive(true);
    }
}
