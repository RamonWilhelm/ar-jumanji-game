﻿using System;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARSubsystems;

public class ChooseFigureButtons : MonoBehaviour
{
    public Button back;
    public Button lionButton;
    public Button horseButton;

    public GameObject placingBoardMenu;
    public GameObject chooseFigureMenu;
    public GameObject playerView;

    public GameObject planeGenerationMenu;

    public GameObject lion;
    public GameObject horse;
    void Start()
    {
        back.onClick.AddListener(Back);
        lionButton.onClick.AddListener(ChooseLion);
        horseButton.onClick.AddListener(ChooseHorse);
    }
    
    public void Back()
    {
        chooseFigureMenu.SetActive(false);
        planeGenerationMenu.SetActive(true);
        SpawnObjectOnPlane.isPlaced = false;
        SpawnObjectOnPlane.SetAllPlanesActive(true);
        SpawnObjectOnPlane.planeManager.detectionMode = PlaneDetectionMode.Horizontal;

    }

    private void Update()
    {
        SpawnObjectOnPlane.planeManager.detectionMode = PlaneDetectionMode.None;

    }

    public void ChooseLion()
    {
        GameObject parent = GameObject.FindGameObjectWithTag("Jumanji2");
        lion = parent.transform.GetChild(9).gameObject;
        lion.transform.position = new Vector3(lion.transform.position.x + 1227, 0.005263f, 0.07650683f);
        //lion.transform.Rotate(0, 0, 0);
        lion.SetActive(true);
        Destroy(parent.transform.GetChild(10).gameObject);
        GetComponent<RandomCube>().Setup(lion);

        chooseFigureMenu.SetActive(false);
        playerView.SetActive(true);
    }
    
    public void ChooseHorse()
    {
        GameObject parent = GameObject.FindGameObjectWithTag("Jumanji2");
        horse = parent.transform.GetChild(10).gameObject;
        horse.SetActive(true);
        Destroy(parent.transform.GetChild(9).gameObject);
        GetComponent<RandomCube>().Setup(horse);
        
        chooseFigureMenu.SetActive(false);
        playerView.SetActive(true);
    }
}