﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

[RequireComponent(typeof(ARRaycastManager))]
public class SpawnObjectOnPlane : MonoBehaviour
{
    private ARRaycastManager raycastManager;
    public static GameObject spawnedObject;
    public static ARPlaneManager planeManager;
    public static bool isPlaced;

    public Camera arCamera;
    
    [SerializeField]
    private int maxPrefabSpawnCount = 0;
    private int placedPrefabCount;

    [SerializeField]
    private GameObject placeablePrefab;
    
    static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();

    private int uiLayer;
    
    private void Awake()
    {
        raycastManager = GetComponent<ARRaycastManager>();
        planeManager = GetComponent<ARPlaneManager>();
        isPlaced = false;
        uiLayer = LayerMask.NameToLayer("UI");
    }

    bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if (Input.GetTouch(0).phase == TouchPhase.Began)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;
    }

    private void Update()
    {
        if (!isPlaced)
        {
            if (!TryGetTouchPosition(out Vector2 touchPosition))
            {
                return;
            }
            
            if (!IsPointOverUIObject(touchPosition) && raycastManager.Raycast(touchPosition, s_Hits, TrackableType.PlaneWithinPolygon) && EventSystem.current.IsPointerOverGameObject() == false)
            {
                var hitPose = s_Hits[0].pose;

                if (hitPose.position.y < 0)
                {
                    if (spawnedObject != null)
                    {
                        Destroy(spawnedObject);
                    }

                    PlaneGenerationMenuButtons.isPlaced = true;
                    spawnedObject = Instantiate(placeablePrefab, hitPose.position,
                        Quaternion.Euler(new Vector3(0, 0, 0)));
                }
            }
        }
    }
    
    public static void SetAllPlanesActive(bool value)
    {
        foreach (var plane in planeManager.trackables)
        {
            plane.gameObject.SetActive(value);
        }
    }
    
    bool IsPointOverUIObject(Vector2 pos)
    {
        if (EventSystem.current.IsPointerOverGameObject())
            return false;

        PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
        eventDataCurrentPosition.position = new Vector2(pos.x, pos.y);
        List<RaycastResult> results = new List<RaycastResult>();
        EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
        return results.Count > 0;

    }
}